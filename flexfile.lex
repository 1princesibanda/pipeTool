
%{
#include <ctype.h>
#include <math.h>
#include <string.h> /* for strcmp(), strcat() and strlen() */

static int multiple = 1;
static int accumSize = YYLMAX;
static int accumAvailable = YYLMAX;
static char * accum = "\0";
void getMoreMemory(char *, int*, int*, int*, yy_size_t *);
int specialID(YYSTYPE* yylval);

%}
	/* Page 28 of pdf and 21 of manual, beginning chpt 10 */
%x instage regNum comment
	/* page 21 of pdf and 14 of manual, chapt7 beginning */
%array
%option noyywrap
%option nodefault
%option warn
%option bison-bridge
%option bison-locations

%top{
}

WHITESPACE [ \t\n]+
SPACE [ \t]+
NUM [0-9]+
ENDLINE \n
DO "do"
WHILE while
ABSANYCHARS ([.]{-}[:\n])+
START start
FINAL final
SET set
PIPE pipe
PARALLEL parallel
SEQUENTIAL sequential
STAGE stage
STAGEID [[:alnum:]]
INPUT input
OUTPUT output
FILE_ file
DIR_ dir
STDOUT stdout
ID [_a-zA-Z]+[_a-zA-Z[:digit:]]*
FILENAME \"[^\"\n]+\"
DIRNAME (\/.+)+
COMMA ,
OPENCURLBR {
CLOSECURLBR {
COLON :



%%
%{
static int exeLine = 0;
static int recur = 0;
static int regurg = 0;
static int trueLine = 0;
int prevContext;
if( accum[0] == '\0' )
{
	accum = (char*)malloc( sizeof(char)*YYLMAX );
	if( accum == NULL )
		printf("Parser out of memory.\n");
}
{ /*printf("bool:%d\n", exeLine);*/ }
%}
#				{ prevContext = YY_START; BEGIN(comment); }
<comment>[^\n]+ 		{ /*printf("comment:%s\n", yytext);*/ } 
<comment>\n 			{ BEGIN(prevContext); } 

<INITIAL>,			{ return COMMA; }
<INITIAL>{SPACE} 
<INITIAL>{ID}/{WHITESPACE}	{ 
					trueLine = 1;
					int id = specialID(yylval);
					if( id == NONSPECIALID )
					{
						yylval->ID = strdup(yytext);
						//printf("fID:'%s'\n", yylval); 
						return ID;
					}
					else
						return id;


			 	}
<INITIAL>{FILENAME} { trueLine = 1; yylval->FILENAME = strdup(yytext); /* printf("fFNAME:%s\n", yylval);*/ return FILENAME; }
<INITIAL>{ID}/":"	{ 
				trueLine = 1; 
				int id = specialID(yylval);
				if( id == NONSPECIALID )
				{
					yylval->STAGEID = strdup(yytext); /*printf("fSTGID:%s\n", yylval);*/ return STAGEID; 
				}
				else
					return id;
}

<INITIAL>":" 		{ 	trueLine = 1;
				if( exeLine )
					exeLine++;
				yylval->COLON = strdup(yytext); 
				if( exeLine == 2 ) /* You got the stageID, colon, the newline, and the number that begins the exeLine */
					BEGIN(instage);
				//printf("f:%s\n", yylval); 
				return COLON; 
			}

<INITIAL>{NUM}		{	trueLine = 1; 
				exeLine++; 
				yylval->NUM = strdup(yytext);
				//printf("fNUM:%s\n", yylval);
				return NUM;
			}

<instage>{	trueLine = 1;
		[^_a-zA-Z[:digit:]\n{}][^_a-zA-Z\n{}]* 	{ 
					yylval->ABSANYCHARS = strdup(yytext);
					//printf("*:'%s'\n", yylval); 
					recur = 1;
					getMoreMemory(accum, &accumAvailable, &accumSize, &multiple, &yyleng);

					strcat(accum, yytext);
					accumAvailable -= yyleng;
				}

		{ID}/"{"	{ 
					if( recur )
					{
						recur = 0;
						yylval->ABSANYCHARS = accum;
						//printf("fRIDr:'%s'\n", yytext); 
						regurg = yyleng;
						accum = "";
						accumSize = accumAvailable;
						for(regurg=yyleng-1; regurg>=0; regurg--)
						{
							//printf("l:%c\n", yytext[regurg]);
							unput(yytext[regurg]);
						}
						//printf("fABS:'%s'\n", yylval); 
						return ABSANYCHARS;
					}
					else
					{
						int id = specialID(yylval);
						if( (id == NONSPECIALID) || (id == STDOUT) )
						{
							yylval->ID = strdup(yytext);
							//printf("fRID:%s\n", yylval); 
							return ID; 
						}
						else
							return id;
					}
				}

		{ID} 	{ 
					recur = 1;
					yylval->ID = strdup(yytext);

					getMoreMemory(accum, &accumAvailable, &accumSize, &multiple, &yyleng);
					strcat(accum, yytext);
					accumAvailable -= yyleng;
					//printf("**:'%s'\n", yylval); 
				}
		"{"		{ BEGIN(regNum); /*printf("******REgnum\n");*/ yylval->OPENCURLBR = strdup(yytext); /*printf("f{:%s\n", yylval);*/ return OPENCURLBR; }

	}

<regNum>{	/* trueLine = 1; *//* not needed since regNum only executes during instage, which already has trueLine set. */
		[ \t]*{NUM}[ \t]* 		{ yylval->NUM = strdup(yytext); /*printf("fNUM_:%s\n", yylval);*/ return NUM; }
		"}"		{ BEGIN(instage); yylval->CLOSECURLBR = strdup(yytext); /*printf("f}:%s\n", yylval);*/ return CLOSECURLBR; }
		/* [^0-9} \t]+ 	{ printf("Expected number, but got %s\n",yytext); return ABSANYCHARS; } */
	}

<INITIAL,instage>\n 	{ 
				/* ie process only when its not end of empty line */
				if( trueLine ) 
				{
					yylval->ENDLINE = strdup(yytext); 
					if( YY_START == instage )
					{
						exeLine = 0;
						BEGIN(INITIAL); 
						if( recur )
						{
							unput('\n');
							yylval->ABSANYCHARS = accum; 
							recur = 0;
							//printf("fABS:%s\n", yylval); 
							//printf("Switch*******\n");
							accum = "";
							accumSize = accumAvailable;
							return ABSANYCHARS;
						}

					}
					trueLine = 0;
					//printf("fENDL\n"); 
					return ENDLINE; 
				}
			} 
	/* Here we return token REJECT_ because we expect the grammar parser to reject this token and indicate syntax error, since REJECT_ is not part of grammar */
<*>[^ \t\n0-9a-zA-Z,":] 		{ 
						printf("Unknown Character(s):'%s'\n", yytext); 
						yylval->REJECT_ = strdup(yytext); 
						return REJECT_; 
					}
%%

void getMoreMemory(char * accum, int * accumAvailable, int * accumSize, int * multiple, yy_size_t * yyleng)
{
	/*
	extern int accumAvailable, accumSize, multiple;
	extern char * accum; 
	*/

	while( (*accumAvailable) < (*yyleng) )
	{
		(*multiple)++;
		(*accumSize) = YYLMAX*(*multiple);
		accum = realloc(accum, (*accumSize) );
		if( accum == NULL )
		{
			printf("Parser failuire: Parser needed more memory for parsing but could not get it.\n");
			exit(0);
		}
		(*accumAvailable)+=YYLMAX;
	}

}

int specialID(YYSTYPE * yylval)
{
	if( strcmp(yytext,"input") == 0 ) 
	{
		yylval->INPUT = strdup(yytext);
		//printf("fINPUT:'%s'\n", yylval);
		return INPUT;
	}
	else
	if( strcmp(yytext,"file") == 0 ) 
	{
		yylval->FILE_ = strdup(yytext);
		//printf("fFILE:'%s'\n", yylval);
		return FILE_;
	}
	else
	if( strcmp(yytext,"dir") == 0 ) 
	{
		yylval->DIR_ = strdup(yytext);
		//printf("fDIR:'%s'\n", yylval);
		return DIR_;
	}
	else
	if( strcmp(yytext,"set") == 0 ) 
	{
		yylval->SET = strdup(yytext);
		//printf("fSET:'%s'\n", yylval->SET);
		return SET;
	}
	else
	if( strcmp(yytext,"pipe") == 0 ) 
	{
		yylval->PIPE = strdup(yytext);
		//printf("fPIPE:'%s'\n", yylval); 
		return PIPE;
	}
	else
	if( strcmp(yytext,"parallel") == 0 ) 
	{
		yylval->PARALLEL = strdup(yytext);
		//printf("fPAR:'%s'\n", yylval);
		return PARALLEL;
	}
	else
	if( strcmp(yytext,"sequential") == 0 ) 
	{
		yylval->SEQUENTIAL = strdup(yytext);
		//printf("fSEQ:'%s'\n", yylval); 
		return SEQUENTIAL;
	}
	else
	if( strcmp(yytext,"start") == 0 ) 
	{
		yylval->START = strdup(yytext);
		//printf("fSTART:'%s'\n", yylval);
		return START;
	}
	else
	if( strcmp(yytext,"stage") == 0 ) 
	{
		yylval->STAGE = strdup(yytext);
		//printf("fSTAGE:'%s'\n", yylval); 
		return STAGE;
	}
	else
	if( strcmp(yytext,"final") == 0 ) 
	{
		yylval->FINAL = strdup(yytext);
		//printf("fFINAL:'%s'\n", yylval);
		return FINAL;
	}
	else
	if( strcmp(yytext,"stdout") == 0 ) 
	{
		yylval->STDOUT = strdup(yytext);
		//printf("fSTDOUT:'%s'\n", yylval);
		return STDOUT;
	}
	else
	if( strcmp(yytext,"output") == 0 ) 
	{
		yylval->OUTPUT = strdup(yytext);
		//printf("fOUTPUT:'%s'\n", yylval);
		return OUTPUT;
	}
	else
	if( strcmp(yytext,"do") == 0 ) 
	{
		yylval->DO = strdup(yytext);
		//printf("fDO:'%s'\n", yylval); 
		return DO;
	}
	else
	if( strcmp(yytext,"while") == 0 ) 
	{
		yylval->WHILE = strdup(yytext);
		//printf("fWHILE:'%s'\n", yylval);
		return WHILE;
	}
	return NONSPECIALID;
}
