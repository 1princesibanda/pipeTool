pipe: pipe.tab.c lex.yy.c
	gcc -lm -o pipe pipe.tab.c

pipe.tab.c: pipe.y
	bison pipe.y

lex.yy.c: flexfile.lex
	flex flexfile.lex

.PHONY:
clean:
	rm -vf lex.yy.c pipe.tab.c pipe
