%code top {
#include <stdio.h>
#include <string.h>
char string[400];
}

/*%glr-parser*/

%locations
%define api.pure full
%define api.value.type union
%define parse.trace
//%printer { printf(yyoutput, "%s", ((char *)$$)); } <char *>;

%code {
void yyerror(YYLTYPE * locp, char const * msg);
int yylex(YYSTYPE* yylval_param,YYLTYPE* yyloc_param);
//int yydebug = 1;
}


%token <char *> NUM

%left <char *> ABSANYCHARS

%token <char *> DO WHILE

%token <char *> START FINAL

%token <char *> SET PIPE PARALLEL SEQUENTIAL

%token <char *> STAGE 
%token <char *> STAGEID

%token <char *> INPUT OUTPUT FILE_ DIR_ STDOUT ID FILENAME

%left <char *> ENDLINE

%token <char *> ENDFILE

%token <char *> COLON 
%token <char *> OPENCURLBR 
%token <char *> CLOSECURLBR 
%token <char *> COMMA 
%token <char *> REJECT_
%token NONSPECIALID 

/* Enumerate all non-terminals */
%type <char *> program initStage finalStage stage exeLine stdExeLine conditionalExeLine programCall simpleCall regulator initdeclarations declarationLine settings settingLine outputLine stages optionalExeLines optionalOutputLine optionalStdExeLines optionalSimpleCalls optionalIO IO

/*
%precedence SHIFTMULTILINES
*/

%%
				/* BASIC LANGUAGE */

program: initdeclarations settings initStage stages finalStage { printf("'%s' '%s' '%s' '%s' '%s'", $1, $2, $3, $4, $5, $5);  }
       /*{ printf("^'%s', '%s', '%s', '%s', '%s'\n", $1, $2, $3, $4, $5); }*/

initStage: START stage { printf("'%s' '%s'", $1, $2);  }

finalStage: FINAL stage { printf("'%s' '%s'", $1, $2);  }

stage: STAGE STAGEID COLON ENDLINE exeLine optionalExeLines 
	{ 
		printf("'%s' '%s' '%s' '%s' '%s' '%s'", $1, $2, $3, $4, $5, $5, $6); 
		 

		if( strcmp($2, "reserved") == 0 )
		{
			printf("At line %d reserve word used.\n", @2.first_line );
		}
	}

exeLine: NUM COLON stdExeLine { printf( "'%s' '%s' '%s'",$1, $2, $3);  }
exeLine: NUM COLON conditionalExeLine { printf( "'%s' '%s' '%s'",$1, $2, $3);  }

stdExeLine: programCall optionalOutputLine { printf("'%s' '%s'", $1, $2);  }

conditionalExeLine: DO  ENDLINE stdExeLine optionalStdExeLines WHILE ENDLINE stdExeLine { printf("'%s' '%s' '%s' '%s' '%s' '%s' '%s'", $1, $2, $3, $4, $5, $5, $6, $7);  }

programCall: simpleCall optionalSimpleCalls  ENDLINE { printf( "'%s' '%s'", $1, $2);  }

simpleCall: ABSANYCHARS regulator { printf( "'%s' '%s'", $1, $2);   }

regulator: ID OPENCURLBR NUM CLOSECURLBR { printf( "'%s' '%s''%s''%s'", $1, $2, $3, $4);  }

				/* LANGUAGE OPTIONALS */

initdeclarations: %empty { $$ = "-"; }
initdeclarations: declarationLine initdeclarations { printf("'%s' '%s'", $1, $2);   }

declarationLine: INPUT FILE_ ID FILENAME ENDLINE { sprintf($$,"'%s' '%s' '%s' '%s' '%s'", strdup($1), $2, $3, $4, $5); printf("Inits:%s\n", $$); }
declarationLine: INPUT DIR_ ID FILENAME ENDLINE { sprintf($$,"'%s' '%s' '%s' '%s' '%s'", strdup($1), $2, $3, $4, $5); printf("Inits:%s\n", $$); }
declarationLine: OUTPUT FILE_ ID FILENAME ENDLINE { printf("'%s' '%s' '%s' '%s' '%s'", $1, $2, $3, $4, $5);  }
declarationLine: OUTPUT DIR_ ID FILENAME ENDLINE { printf("'%s' '%s' '%s' '%s' '%s'", $1, $2, $3, $4, $5);  }

settings: %empty { $$ = "-"; }
settings: settingLine { $$ = $1; }

settingLine: SET PIPE PARALLEL ENDLINE { printf("*%s* '%s' '%s' '%s'", $1, $2, $3, $4);sprintf($$,"*%s* '%s' '%s' '%s'", $1, $2, $3, $4); /*printf("Inits:%s\n", $$);*/ }
settingLine: SET PIPE SEQUENTIAL ENDLINE { printf("'%s' '%s' '%s' '%s'", $1, $2, $3, $4);  }

stages: %empty { $$ = "-"; }
stages: stage stages { printf("'%s' '%s'", $1, $2);  }

optionalExeLines: %empty { $$ = "-"; }
optionalExeLines: exeLine optionalExeLines { printf("'%s' '%s'", $1, $2);  }

outputLine: OUTPUT IO optionalIO ENDLINE { printf( "'%s' '%s' '%s' '%s'", $1, $2, $3, $4);  }

optionalOutputLine: %empty { $$ = "-"; }
optionalOutputLine: outputLine { $$ = $1; }

optionalStdExeLines: %empty { $$ = "-"; }
optionalStdExeLines: stdExeLine optionalStdExeLines { printf( "'%s' '%s'", $1, $2); }

optionalSimpleCalls: %empty { $$ = "-"; }
optionalSimpleCalls: ABSANYCHARS optionalSimpleCalls { printf("'%s' '%s'", $1, $2);  }
optionalSimpleCalls: regulator optionalSimpleCalls { printf("'%s' '%s'", $1, $2);  }
				/* LANGUAGE OPTIONALS OPTIONS */
optionalIO: %empty { $$ = "-"; }
optionalIO: COMMA IO optionalIO { printf("'%s' '%s' '%s'", $1, $2, $3);  }

IO: FILE_ ID FILENAME { printf("'%s' '%s' '%s'", $1, $2, $3);  }
IO: DIR_ ID FILENAME { printf("'%s' '%s' '%s'", $1, $2, $3);  }
IO: STDOUT { $$ = $1; }

%%

#include "lex.yy.c"
/*
int yylex(void)
{
}
*/
char * filename;

void yyerror(YYLTYPE * locp, char const * msg)
{
	printf("%s:%d:%d: Error:%s\n", filename, locp->last_line, locp->last_column, msg);
}

int main(void)
{
	filename = "f";
	yyin = fopen(filename, "r");
	yyparse();
	return 0;
}
